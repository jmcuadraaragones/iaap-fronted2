const gulp = require('gulp');
const { series } = require('gulp');
const clean = require('gulp-clean');
const browserSync = require('browser-sync').create();
const nunjucksRender = require('gulp-nunjucks-render');


function nunjucks() {
  return gulp.src('./src/**/*.html')
    .pipe(nunjucksRender({
        envOptions: {
          autoescape: true,
          trimBlocks: true,
          lstripBlocks: true
        },
        path: './src/templates/' // String or Array
      }))
    .pipe(gulp.dest('./dist/'));
}




function bs(cb) {
  browserSync.init({
    server: {
      baseDir: './dist/'
    }
  });

  gulp.watch('./src/**/*', gulp.series(copyFile, nunjucks, reload));
  cb();
}


function reload(cb) {
  browserSync.reload();
  cb();
}



function cleanFolder() {
  return gulp.src('./dist/', {read: false, allowEmpty: true})
    .pipe(clean());
}


function copyFile() {
  return gulp.src('./src/**/*')
    .pipe(gulp.dest('./dist/'));
}



function defaultTask(cb) {
  console.log('Hola gulp');
  // place code for your default task here
  cb();
}

exports.default = series(defaultTask, cleanFolder, copyFile, nunjucks, bs);
